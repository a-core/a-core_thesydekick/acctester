# Adding custom instruction to RISC-V GNU toolchain

This guide explains how to utilize custom accelerators from C.
The process consists of three main steps:

1. Modify instruction decoder to recognize binary chosen for the custom accelerator
2. Modify GNU assembler to map custom assembly instruction to the chosen binary
3. Add a C wrapper function for the assembly instruction

## 1. Prerequisites

This tutorial assumes that you already have a working [a-core_thesydekick](
https://gitlab.com/a-core/a-core_thesydekick/a-core_thesydekick) environment
on your machine. Note that this tutorial has only been tested on my local
machine, so some steps might differ on your machine.

Some changes also need to made to [a_core_common](
https://gitlab.com/a-core/a-core_chisel/a_core_common) project. This project
is defined as a dependency of the a-core_thesydekick project. This project needs
to be cloned separately, and you need to be able to publish changes to a local ivy
repository.

### 1.1 Compiling the RISC-V GNU toolchain

This tutorial includes modifying the RISC-V GNU toolchain, hence it needs to
be compiled from source. The A-Core hosts a non-standard set of extensions.
The following instructions include a correct configuration for the processor.

**Note that the installation can take a very long time (>1 hour).**

1. Clone the repository from https://github.com/riscv-collab/riscv-gnu-toolchain.
2. Open the README in that repository
3. Ensure you have the prerequisites
4. Run `./configure --prefix=/prog/riscv --with-multilib-generator="rv32i-ilp32--;rv32im-ilp32--;rv32if-ilp32--;rv32imf-ilp32--;rv32if-ilp32f--;rv32imf-ilp32f--"`. **NOTE**: change the prefix to a path where you have write access to, e.g. `$HOME/opt/riscv` !
5. Run `make` (You might want to use `make -j$(nproc)` for faster compilation time)
6. Add the binaries to PATH after finished.

### 1.2 Understanding the RISC-V instruction structure

The official RISC-V specification introduces four core instruction formats (R/I/S/U)
and two further variants (B/J), where B-type is a variant of S-type and J-type is a
variant of U-type. All the different formats are shown in the following figure [^fn1].

![](images/instruction_formats.png)

The specific instruction is identified with opcode and funct3/funct7 fields if used
in the instruction format. The rs1 and rs2 fields contain addresses for source
registers and rd contains address for destination register. The imm fields are used
for immediate values. The RISC-V specification reserves different opcodes for groups
of instructions as shown in the following figure[^fn1]. Note that this table assumes
that lowest two bits of the opcode are always inst[1:0]=11, hence not shown in the
table.

![](images/opcode_table.png)

This way the specification reserves instructions starting with for example opcode
inst[6:0]=0110011 for "OP" type instructions which are all in the same instruction
format, in this case R-type. These include for example ADD and SUB operations. The
specification also reserves some parts for custom instructions. Namely, custom-0 to
custom-3 can be used for this.

It is advised to use one opcode for one type of instruction
format because this makes implementing instruction decoder simpler. For example
the core formats (R/I/S/U) could be used with custom-0 to custom-3 opcodes respectively.
This means that if we choose to use custom-0 opcode for R-type instructions, we
still have funct3 and funct7 bits to use for determining the exact instruction.
So in this case we would be able to encode 2<sup>3+7</sup> distinct custom R-type
instructions.

Refer to the official RISC-V specification for more details.


## 2. Adding the custom instruction

This guide uses a very simple custom accelerator as an example. This custom
accelerator is called add_incr. It adds two numbers together and increments
the result by one.


### 2.2 Modifying A-Core
#### 2.2.1 Modifying the a_core_common

In this example we will add the instruction as a part of the ALU. The
a_core_common defines some enumeration types that are used by the ALU to
differentiate between different instructions. We want to add our own enum
representing the add_incr instruction here. Navigate to
`a_core_common/src/main/scala/a_core_common` and open file
`a_core_common.scala` for editing. Add custom enumeration type `ADD_INCR`
inside the `object ALUOp extends ChiselEnum`. Remember to run `sbt
publishlocal` in the project root after this.

#### 2.2.2 Modifying ALU

Now we can add the custom instruction to the a-core project. Assuming you have
a clean a-core_thesydekick project open, navigate to
`a-core_thesydekick/Entities/ACoreChip/chisel/ACoreBase/src/main/scala/acorebase`.
For the sake of example, let us modify ALU to calculate our custom add_incr
operation. In a real implementation, the custom accelerator would probably be
placed outside the core ALU in some place grouped together with other custom
accelerators.

Open file `alublock/alu.scala`. Main part of this file consists of a switch
statement which calculates different operations based on enumeration value.
Add the implementation for our add_incr operation inside the switch statement
along with other "is" blocks.

```scala
// ...
is (ALUOp.ADD_INCR) {
  io.out := (io.a + io.b) + 1.U
}
// ...
```

#### 2.2.3 Modifying instruction decoder

Now we need to choose the opcode used for our add_incr instruction. This will
be R-type instruction since it uses two source registers and one destination
register. Let us use custom-0 field for this. This means that our opcode is
0001011 based on the previous figures. Type R-instruction includes both funct3
and funct7 fields so we can choose any values for these to refer to our add_incr
instruction. Let us choose funct3=000 and funct7=0000000. This means that our
instruction will be of form
```text
0000000xxxxxyyyyy000zzzzz0001011
```
where x's, y's and z's stand for source1, source2 and destination registers'
addresses respectively.

Now, open file `control.scala`. This includes a big when-elsewhen block. This
recognizes the opcode, performs the needed operations for each opcode type, and
finally checks the relevant funct3/funct7 fields to recognize the specific
instruction. We want to recognize all add_incr instructions so we will add a
separate elsewhen block for that purpose.

In this example there is no check for custom-0 instructions yet, so we will add
one. In this example we also choose to use all custom-0 instructions for R-type
instructions because this makes implementation simpler. Add the following block
in the big when-elsewhen block:

```scala
// ...
} .elsewhen (io.opcode === Opcode.custom_0) {
  // These are copied from Opcode.OP which is also
  // R-type instruction.
  io.regfile_block_write.wr_en        := true.B
  io.regfile_block_write.wr_in_select := RegFileSrc.ALU_RESULT
  io.alu_block.operand_a_src := AluOperandSrc.RS1
  io.alu_block.operand_b_src := AluOperandSrc.RS2

  // Here we identify the add_incr instruction
  when (io.funct3 === "b000".U && io.funct7 === "b0000000".U) {
    io.alu_block.operation := ALUOp.ADD_INCR
  } .otherwise {
    io.illegal_instr := true.B
  }
}
// ...
```

### 2.3 Modifying GNU assembler

This part assumes you have cloned and built the repository as explained in
the prerequisites section. Navigate to your riscv-gnu-toolchain directory that
you cloned from GitHub. We want to modify the GNU assembler, which maps
assembly instructions to corresponding binaries. The assembler is part of
GNU binary utilities. Navigate to directory `binutils`. Open file
`opcodes/riscv-opc.c`.

This file contains array `const struct riscv_opcode riscv_opcodes[]`.
This array contains structs describing every known assembly instruction.
We want to add struct here that describes our add_incr instruction. The
`struct riscv_opcode` consists of the following fields
```c
// Name of the instruction that can be recognized in assembly,
// for example add
const char *name;

// Represents instruction length, for exampxle 32 or 64.
// If instrucion can be used with both 32 and 64 bit
// architectures, this can be set to 0
unsigned xlen_requirement;

// Class to which this instruction belongs. For example
// INSN_CLASS_I means it belongs to the standard base
// integer instruction set
enum riscv_insn_class insn_class;

// String that represent what arguments the instruction can take.
// This will be used when parsing the assembly file. For example
// "d,s,t" means that the instruction takes two source register
// addresses and one destination register address separated with
// commas
const char *args;

// Match is a predefined constant, for example "#define MATCH_ADD 0x33"
// where the "0x33" represents the full opcode. For example in 32 bit
// architecture this includes the the full 32 bits. However, not all
// parts of the full opcode are relevant to determining the specific
// instruction, because some fields include information for register
// addresses for example. This fields can be set to zeros.
insn_t match;

// The mask tells which parts of match are actually relevant for
// determining the specific instruction. Parts that are relevant are
// set to ones and rest are set to zeros. This is also a predefined
// constant, for example "#define MASK_ADD 0xfe00707f"
// (This can also have other meaning if pinfo is INSN_MACRO, check
// the source code for more information)
insn_t mask;

// This is a function that determines if a word corresponds to this
// instruction. Normally this just computes ((word & mask) == match)
int (*match_func) (const struct riscv_opcode *op, insn_t word);

// This is INSN_MACRO for any macros. For other types it is a collection
// of bits that describe the instruction. Any relevant hazard information
// for example is included in here.
unsigned long pinfo;
```

In our example:

* **name** is **"add_incr"**
* **xlen** is **32**
* **insn_class** is **INSN_CLASS_I**. Note that normally we would
  probably want to define a custom instruction class and require a
  specific flag for compiler to include our custom instruction in
  the instruction set. However, for the sake of simplicity we choose
  to include our custom instruction as a part of base integer instruction
  set here.
* **args** is **"d,s,t"**
* **match** is **MATCH_ADD_INCR**. We will define this in a related header
  file in a moment.
* **mask** is **MASK_ADD_INCR**. We will define this in a moment as well.
* **match_func** is **match_opcode**. This is the same for most instructions
  and is already defined for us.
* **pinfo** is **0**. According to [this](https://pcotret.gitlab.io/riscv-custom/sw_toolchain.html) tutorial, this field is only non-zero for branch/jump instructions.

Now, add the following struct inside the `riscv_opcodes[]` array before the
last item that terminates the array

```c
// ...
{"add_incr", 32, INSN_CLASS_I, "d,s,t", MATCH_ADD_INCR, MASK_ADD_INCR, match_opcode, 0},
// ...
```

Now we need to define the MATCH_ADD_INCR and MASK_ADD_INCR. Close the file and
make sure you are back in the `riscv-gnu-toolchain/binutils` directory. Then open
file `include/opcode/riscv-opc.h`. Add the following lines together with other
defines
```c
#define MATCH_ADD_INCR 0xb
#define MASK_ADD_INCR  0xfe00707f
```
As mentioned earlier, our instruction is of form 0000000xxxxxyyyyy000zzzzz0001011,
where x's, y's and z's stand for register addresses. If we set all of those to
zeros, we get binary number 1011 (with many zeros prepended). This corresponds
to hexadecimal 0xb and is our match.

For the mask we need to set all the x's, y's and z's to zero, because they are
not relevant when figuring out which instruction is in question. However, all
the other bits are relevant, so they have to be set to ones. This way we get a
binary number, which corresponds with hexadecimal 0xfe00707f which is our mask.

After this we still need to add one more line in the same header file to declare
our instruction. Add the following line together with other declarations
```c
DECLARE_INSN(add_incr, MATCH_ADD_INCR, MASK_ADD_INCR)
```

Now we are ready to recompile our binutils. Go back to your project root and
navigate to directory `riscv-gnu-toolchain/build-binutils-newlib/binutils`
now run
```shell
make
sudo make install
```

### 2.4 Creating a C wrapper function

We can use extended asm which is a GNU extension. This allows using inline
assembly in C and particularly using C variables in the assembly. The
syntax is following
```c
int a, b, c;
asm ("add_incr %0, %1, %2"
     : "=r"(c)
     : "r"(a),
       "r"(b));
```
In this case %0 refers to variable c. %1 refers to a and %2 to b. The r's
mean that we want the values to be stored in registers. The equal
sign means that c in this case is the destination register.
Refer to the [GNU website](https://gcc.gnu.org/onlinedocs/gcc/Extended-Asm.html)
for more detailed information.

Now we want to make a function that takes two unsigned integers and returns
the sum incremented by one. We can do it like this
```c
unsigned add_incr(unsigned a, unsigned b) {
    int ret;
    asm ("add_incr %0, %1, %2"
         : "=r"(ret)
         : "r"(a),
           "r"(b));
    return ret;
}
```

#### 2.4.1 Notes about the wrapper function

Some tutorial about this topic proposed using macros for inline assembly
instead of using a wrapper function. By itself this would probably create
less overhead because we don't have to create the input and output variables
and the function call. However, I think from programmers perspective this is
harder to use.

If the implementation with a C wrapper function is compiled without optimizations,
this will generate **a lot** of extra instructions in the assembly subroutine.
Using -Os optimizations seems to optimize the subroutine to a single assembly
instruction plus return from the subroutine. However, this was only tested with
a couple very simple programs.

There might still be the overhead of calling a subroutine and returning from it,
unless the compiler is able to optimize this away? Making the function inline
might help?

## 3. Testing

We will use a-core_thesydekick to test if we are now able to utilize our
custom accelerator using C. First, we want to copy a test template to a
new directory. Navigate to directory
`a-core_thesydekick/Entities/ACoreTests/sw/riscv-c-tests/src`.
Copy the template to new location

```shell
cp -r riscv-c-template accelerator-test
```

Then, open the file `accelerator-test/main.c` for editing, and replace all 
of the contents with

```c
#include <stdio.h>
#include "a-core-utils.h"

unsigned add_incr(unsigned a, unsigned b) {
    int ret;
    asm ("add_incr %0, %1, %2"
         : "=r"(ret)
         : "r"(a),
           "r"(b));
    return ret;
}

void main() {
    unsigned a = add_incr(1, 2);
    printf("Result: %u", a);
    test_pass();
    test_end();
}
```

Navigate back in directory structure to `a-core_thesydekick/Entities/ACoreTests`.
Copy the c-template configuration file with

```shell
cp tests/test-configs/c-template.yml tests/test-configs/accelerator-test.yml
```

Open the `tests/test-configs/accelerator-test.yml` for editing and change the
line starting with `test_program:`. Change `riscv-c-template` to `accelerator-test`.
Now you can run the tests with

Run
```shell
./configure
make chisel
make test_config=accelerator-test simulator=thesdk backend=icarus
```

Now we should get an output of (1+2)+1 = 4.

#### 3.1 Notes about testing

The `add_incr` function was included in the same source file here for simplicity.
When the implementation gets distributed, this kind of extensions would be placed
in dedicated source and header files.

You should also check that the assembly and binary files produced from the
compilation make sense. You can compile to assembly with
```shell
riscv64-unknown-elf-gcc -march=rv32i -mabi=ilp32 -Os -S <source_file_name>
```
and on the other hand when you have a binary after normal compilation, you can
inspect the binary with
```shell
riscv64-unknown-elf-objdump -d <binary_file_name>
```

## 4. Final notes / TODO

This tutorial shows an example of adding a simple instruction to RISC-V GNU
toolchain and A-Core. When adding different kind of instructions, some parts
might need to be modified. Some things need to be considered on a case by case
basis. This tutorial should still work as a starting point for adding any
custom instructions to this toolchain.

The example in this guide uses the custom instruction as a part of the base
integer instruction set. This means that the added C function is available
when compiling with `-march=rv32i` flag. At some point we would probably want
to make a custom extension with some name (that follows the naming conventions
defined in riscv specification) like xvendorname. Then we would want to be
able to give a flag `-march=rv32i_xvendorname` to the compiler and only then
allow the use of these added instructions. Seems like file
`riscv-gnu-toolchain/binutils/bfd/elfxx-riscv.c` contains relevant information,
but I wasn't able to get this to work.

## References

[^fn1]: The RISC-V Instruction Set Manual, Volume I: Unprivileged ISA,
Document Version 20191213

